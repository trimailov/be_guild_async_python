class Map:
    def __init__(self, function, iterable):
        self.function = function
        self.iterable = iterable
        self.idx = 0  # track state of the looping

    def __iter__(self):
        # `__iter__()` must return iterable object, i.e. object which
        # has `__next__()` method
        return self

    def __next__(self):
        try:
            result = self.function(self.iterable[self.idx])
        except IndexError:
            raise StopIteration
        self.idx += 1
        return result


lst = [1, 2, 3, 4]


def square(a):
    return a * a


print("Let's start")

map_new = Map(square, lst)
for i in map_new:
    print(i)  # 1 4 9 16


print("\nIs iterator exhausted?")
# iterator is exhausted and thus empty
for i in map_new:  # does not loop, and thus does not print
    print(i)


print("\nRun stdlib's map()")
# stdlib's `map()` works identically
map_orig = map(square, lst)
for i in map_orig:
    print(i)


print("\nIs map exhausted?")
# iterator is exhausted and thus empty
for i in map_orig:
    print(i)


# lists are not iterators
print("\nLists are not iterators!")
assert '__iter__' in dir(lst)
assert '__next__' not in dir(lst)


# tuples are not iterators
tpl = (1, 2, 3, 4, 5)
print("\nTuples are not iterators!")
assert '__iter__' in dir(tpl)
assert '__next__' not in dir(tpl)


# string are not iterators
strg = 'string'
print("\nStrings are not iterators!")
assert '__iter__' in dir(strg)
assert '__next__' not in dir(strg)


# but in loops lists are transformed to iterators
lst_iter = lst.__iter__()
print(lst_iter)  # <list_iterator at 0x10beb4370>
print("\nList iterators are iterators!")
assert '__iter__' in dir(lst_iter)
assert '__next__' in dir(lst_iter)
