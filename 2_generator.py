def map_(function, iterable):
    for i in iterable:
        yield function(i)


lst = [1, 2, 3, 4]


def square(a):
    return a * a


print("Let's start")

# generator function does not run when initiated
# it just returns the generator iterator objects
map_new = map_(square, lst)
print(map_new)  # <generator object map_ at 0x10bc1b660>
for i in map_new:
    print(i)


print("\nIs generator exhausted?")
# generator is exhausted and thus empty
for i in map_new:
    print(i)


print("\nRun stdlib's map()")
# stdlib's `map()` works identically
map_orig = map(square, lst)
for i in map_orig:
    print(i)


print("\nIs map exhausted?")
# iterator is exhausted and thus empty
for i in map_orig:
    print(i)


print("\nFor loop is a syntactic sugar for while loop?")
# for loop is syntactic sugar for a while loop?
map_new = map_(square, lst)
iter_map = map_new.__iter__()  # get iterator from our iterable
while True:
    try:
        value = next(iter_map)  # next(obj) calls obj.__next__()
    except StopIteration:
        break
    # for loop's body:
    print(value)


print("\nGenerator function produces a generator iterator object!")
# prove to ourselves that `yield` is a syntactic sugar to produce object
# with `__iter__` and `__next__` objects
assert '__iter__' in dir(map_new)
assert '__next__' in dir(map_new)
