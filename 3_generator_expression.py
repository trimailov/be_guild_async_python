lst = [1, 2, 3, 4]


def square(a):
    return a * a


# generator can be an expression (comprehension?)
square_gen = (square(i) for i in lst)

for i in square_gen:
    print(i)
