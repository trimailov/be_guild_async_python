from inspect import getgeneratorstate


def map_(function, iterator):
    for i in iterator:
        yield function(i)


lst = [1, 2, 3, 4]


def square(a):
    return a * a


print("We can actually iterate through the generator manually")
map_new = map_(square, lst)
print("State: ", getgeneratorstate(map_new))
next(map_new) # 1
print("We iterated `map_new` once")
print("State: ", getgeneratorstate(map_new))
next(map_new) # 4
print("We iterated `map_new` twice")
print("State: ", getgeneratorstate(map_new))
print("Look mah, I am doing stuff before it ran again!")
next(map_new) # 9
print("We iterated `map_new` three times")
next(map_new) # 16
print("We iterated `map_new` four times")
try:
    next(map_new) # StopIteration
except StopIteration:
    print("Generator exhausted!")
    print("State: ", getgeneratorstate(map_new))
