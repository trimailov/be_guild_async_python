# Strongly influenced by: http://www.dabeaz.com/generators/Generators.pdf
import io


log_file = io.StringIO()
log_file.write('[2021-11-20 12:34:56] "GET /index.html HTTP/1.1" 200 12345\n')
log_file.write('[2021-11-20 12:43:42] "GET /users/ HTTP/1.1" 200 5678\n')
log_file.write('[2021-11-20 13:00:12] "GET /users/ HTTP/1.1" 200 5678\n')
log_file.write('[2021-11-20 13:55:00] "POST /admin/ HTTP/1.1" 200 98976\n')
log_file.write('[2021-11-20 14:34:42] "GET /index.html HTTP/1.1" 200 12345\n')
log_file.write('[2021-11-20 17:34:13] "POST /admin/ HTTP/1.1" 200 98976\n')
log_file.write('[2021-11-21 01:09:30] "GET /admin/ HTTP/1.1" 200 9876\n')
log_file.seek(0)

# Total bytes: (2*12345)+(2*5678)+(2*98976)+9876 = 243874


def byte_col(log_file):
    for line in log_file:
        print("Here you go a byte column!")
        yield line.split()[-1]


def byte_val(byte_cols):
    for byte in byte_cols:
        print("Here you go a byte value!")
        yield int(byte)


# Pipeline: log_file -> byte_col -> byte_val -> sum()
total_bytes = sum(byte_val(byte_col(log_file)))
print(total_bytes)
