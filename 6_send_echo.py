# From https://docs.python.org/3/reference/expressions.html?highlight=send#examples

def echo(value=None):
    print("Execution starts when 'next()' is called for the first time.")
    try:
        while True:
            try:
                value = (yield value)  # this makes a coroutine
            except Exception as e:
                value = e
    finally:
        print("Don't forget to clean up when 'close()' is called.")


generator = echo(1)
try:
    generator.send(10)  # TypeError, because generator was not started (primed)
except TypeError:
    print("Generator not started yet!")

print("Start generator")

print(next(generator))  # Primes coroutine, yields 1, because this value was initiated with generator
print(next(generator))  # None

print("Send value to generator")
print(generator.send(2))  # 2

print("Handle error sent to generator")
print(generator.throw(TypeError, "spam"))  # spam
generator.close()
generator.send(5)  # StopIteration
