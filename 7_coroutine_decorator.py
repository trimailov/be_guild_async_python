# Decorator to prime our generator coroutines automatically
from functools import wraps


def coroutine(gen):
    @wraps(gen)
    def inner(*args, **kwargs):
        g = gen(*args, **kwargs)
        next(g)
        return g
    return inner


@coroutine
def echo():
    value = None
    try:
        while True:
            try:
                value = yield value  # coroutine
                yield value  # generator
            except Exception as e:
                value = e
    finally:
        print("Don't forget to clean up when 'close()' is called.")


generator = echo()
print(generator.send(10))  # No error, because it's already primed
print(next(generator))  # None
print(generator.send(2))  # 2
print(generator.throw(TypeError, "spam"))  # spam
generator.close()
