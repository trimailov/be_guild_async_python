# Highly influenced from: http://www.dabeaz.com/coroutines/Coroutines.pdf
import socket
from functools import wraps


def coroutine(gen):
    @wraps(gen)
    def inner(*args, **kwargs):
        g = gen(*args, **kwargs)
        next(g)
        return g
    return inner


def read_socket(target):
    sock = socket.socket()
    sock.connect(('localhost', 1234))
    while True:
        received = str(sock.recv(1024), "utf-8")
        target.send(received)


@coroutine
def grep(pattern, target):
    while True:
        line = (yield)
        if pattern in line:
            target.send(line)


@coroutine
def printer():
    while True:
        line = (yield)
        print(line)


# Consume data from a socket and push it through our grep pipeline
# read_socket -> grep('python') -> printer()
read_socket(grep('python', printer()))

# Use: launch `nc -l 1234` on a separate terminal, launch this script,
# then from `netcat` send data to our pipeline
