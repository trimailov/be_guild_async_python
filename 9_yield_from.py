# `yield from` delegates subgenerators straight to the caller
# Allowed only inside functions

def generator():
    yield 1
    yield 2
    yield 3


def consume_generator():
    # Equivalent to a looping through the generator
    #     for i in generator():
    #         yield i
    yield from generator()


for i in consume_generator():
    print(i)


# Classic example is chaining iterators
def chain(*args):
    for i in args:
        yield from i


for c in chain([1, 2, 3], [4, 5, 6], chain('ABC', 'DEF')):
    print(c)
