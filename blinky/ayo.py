# https://www.youtube.com/watch?v=CRPnkTv1phs
# Petr Viktorin: Building an async event loop

import time


numbers = [0, 0]

async def fib(number, delay, maximum=0):
    a = b = 1
    while a < maximum:
        a, b = b, (a + b)
        numbers[number] = a
        await sleep(delay)


class sleep:
    def __init__(self, delay):
        self.delay = delay

    def __await__(self):
        yield self.delay/2
        yield self.delay/2


tasks = []


number = 0
def create_task(task):
    global number
    number += 1
    task = 0, number, task.__await__()
    tasks.append(task)
    return sleep(0)


def run(task):
    create_task(task)
    current_time = 0
    while tasks:
        tasks.sort()
        task = tasks.pop(0)
        task_time, number, iterator = task

        if task_time > current_time:
            time.sleep(task_time - current_time)
            current_time = task_time

        try:
            delay = next(iterator)
        except StopIteration:
            pass
        else:
            task = task_time + delay, number, iterator
            tasks.append(task)
