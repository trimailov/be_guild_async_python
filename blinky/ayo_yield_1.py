# https://www.youtube.com/watch?v=CRPnkTv1phs
# Petr Viktorin: Building an async event loop

def fib(maximum=0):
    a = b = 1
    while a < maximum:
        yield a
        a, b = b, (a + b)


iterator = iter(fib(maximum=10_000))


# Equivalent to:
# for i in fib():
#     print(i)
while True:
    try:
        i = next(iterator)
    except StopIteration:
        break
    print(i)
