# https://www.youtube.com/watch?v=CRPnkTv1phs
# Petr Viktorin: Building an async event loop

numbers = [0, 0]

def fib(number, maximum=0):
    a = b = 1
    while a < maximum:
        yield a
        a, b = b, (a + b)
        numbers[number] = a


iterators = [
    iter(fib(0, maximum=10_000)),
    iter(fib(1, maximum=10_000)),
]


# Equivalent to:
# for i in fib():
#     print(i)
while True:
    for iterator in iterators:
        try:
            i = next(iterator)
        except StopIteration:
            break
        print(numbers)
