# https://www.youtube.com/watch?v=CRPnkTv1phs
# Petr Viktorin: Building an async event loop

import time


numbers = [0, 0]

def fib(number, delay, maximum=0):
    a = b = 1
    while a < maximum:
        a, b = b, (a + b)
        numbers[number] = a
        yield from sleep(delay)


def sleep(delay):
    yield delay/2
    yield delay/2


tasks = [
    (0, 1, iter(fib(0, 1/10, maximum=10_000))),
    (0, 2, iter(fib(1, 1/3, maximum=10_000))),
]


current_time = 0

while tasks:
    tasks.sort()
    task = tasks.pop(0)
    task_time, number, iterator = task

    if task_time > current_time:
        time.sleep(task_time - current_time)
        current_time = task_time

    try:
        delay = next(iterator)
    except StopIteration:
        pass
    else:
        task = task_time + delay, number, iterator
        tasks.append(task)

    print(numbers)
