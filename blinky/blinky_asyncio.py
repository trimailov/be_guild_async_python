# https://www.youtube.com/watch?v=CRPnkTv1phs
# Petr Viktorin: Building an async event loop

import ayo as async_lib
import random
import time


def print_blinkies(family):
    for blinky in family:
        print(blinky, end=' ')
    print(end='\r')


class Blinky:
    def __init__(self, family):
        self.face = '(o.o)'
        self.family = family

    def __str__(self):
        return self.face

    async def show_face(self, new_face, delay):
        self.face = new_face
        print_blinkies(self.family)
        await async_lib.sleep(delay)

    async def run(self):
        while True:
            await self.show_face('(-.-)', 0.1)
            await self.show_face('(o.o)', random.uniform(0.1, 1.5))


family = []
family.extend(Blinky(family) for i in range(10))


async def run_all():
    tasks = []
    for blinky in family:
        tasks.append(async_lib.create_task(blinky.run()))
    for task in tasks:
        await task


async_lib.run(run_all())
