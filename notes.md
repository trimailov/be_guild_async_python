# Attempt to understand asynchronous python

This topic may be confusing or hard to understand for python beginners.

## Goals and motivation

- Title - attempt to grok `asyncio`;
- People are talking that `yield from` makes `asyncio` work, but I should not
  worry about it..;
- Why some python functions (`map()`) are so confusing?
    - Running a `map` function does not __run__ it;
    - After looping through it, it does not "work" anymore. Wat?

## This is a rabbit hole

<alice in wonderland.jpg>

await, async def, __await__, coroutines, yield from, yield, generators, send(), iterators, __iter__,

## History

Python history on coroutines ( https://en.wikipedia.org/wiki/Coroutine#Python ):
- Python2.2, PEP 255 ( https://www.python.org/dev/peps/pep-0255/ ):
    - this happened in 2001 - 20 years ago!
    - `yield` statement introduced;
- Python2.5, PEP 342 ( https://www.python.org/dev/peps/pep-0342/ ):
    - `yield` becomes an expression and not a statement, i.e. `result = yield` is possible;
    - `send()` function to send values to generator;
    - `throw()` function to send exceptions to generator;
    - `close()` function to send `GeneratorExit` to generator;
- Python3.3, PEP 380 ( https://www.python.org/dev/peps/pep-0380/ ):
    - `yield from` - allows to yield values from other iterators (subgenerator delegation);
- Python3.4, PEP 3156 ( https://www.python.org/dev/peps/pep-3156/ ):
    - `asyncio` library (event loop, protocols, transports, scheduler, etc.):
        - How genertors are related to `asyncio`? That's what I am looking for.
- Python3.5, PEP 492 ( https://www.python.org/dev/peps/pep-0492/ ):
    - `async/await`;
    - `async def` = coroutine function;
    - `await` = basically a `yield from` with strictness to awaitable objects
      (coroutines, `types.coroutine()`, `__await__()`);

## What's an iterator?

iterator (https://docs.python.org/3/glossary.html#term-iterator)
>    An object representing a stream of data. Repeated calls to the iterator’s
>    `__next__()` method (or passing it to the built-in function `next()`) return
>    successive items in the stream. When no more data are available a `StopIteration`
>    exception is raised instead. At this point, the iterator object is exhausted
>    and any further calls to its `__next__()` method just raise `StopIteration` again.
>    Iterators are required to have an `__iter__()` method that returns the iterator
>    object itself so every iterator is also iterable and may be used in most places
>    where other iterables are accepted. One notable exception is code which
>    attempts multiple iteration passes. A container object (such as a list)
>    produces a fresh new iterator each time you pass it to the iter() function or
>    use it in a for loop. Attempting this with an iterator will just return the
>    same exhausted iterator object used in the previous iteration pass, making it
>    appear like an empty container.

## Code example

1_iterator.py

## TL;DR:

Iterators must have `__iter__()` and `__next__()` methods:
- `__iter__()` - returns the iterator object;
- `__next__()` - returns next piece of data or `StopIteration` if exhausted:
    - Builtin function `next(obj)` is equivalent to `obj.__next__()`;

## Iterable vs Iterator

Iterables are objects ("containers" in python docs) that can return iterators
by using `iter()` function or `.__iter__()` method. In short iterables do not
define `__next__()` method and thus throw an error on `next()` function calls.

Iterables also have `__getitem__()` method.

Examples: lists, tuples, `range()`, strings - they are iterables and not iterators.

My guess is that the reason is defined at the end of `iterator` definition,
i.e. during looping - new iterator object is produced from iterable, thus we
can loop iterable many times without worrying about it "exhausting".

## What's a generator?

generator (https://docs.python.org/3/glossary.html#term-generator)
>    A function which returns a generator iterator. It looks like a normal
>    function except that it contains `yield` expressions for producing a series
>    of values usable in a for-loop or that can be retrieved one at a time with
>    the `next()` function.

generator iterator is an object returned by generator;

## Code example

2_generator.py
3_generator_expression.py
4_generator_next.py

## Wat?

From `4_generator_next.py` we now know that generators suspend executing at
`yield` statement. We can resume execution until next `yield` with `next()`.

From https://docs.python.org/3/reference/expressions.html#yieldexpr :
>    By suspended, we mean that all local state is retained, including the current
>    bindings of local variables, the instruction pointer, the internal evaluation
>    stack, and the state of any exception handling. When the execution is resumed
>    by calling one of the generator’s methods, the function can proceed exactly as
>    [if](if) the yield expression were just another external call.

## Inspect

Stdlib's `inspect` package provides tools to look into generator state:
https://docs.python.org/3/library/inspect.html?highlight=inspect#inspect.getgeneratorstate

    Possible states are:
        GEN_CREATED: Waiting to start execution.
        GEN_RUNNING: Currently being executed by the interpreter.
        GEN_SUSPENDED: Currently suspended at a yield expression.
        GEN_CLOSED: Execution has completed.

## Use cases

- Save memory;
- Process big data streams:
    - Because `yield` suspends the iterator and produces a value, we can
      process huge data in chunks (e.g. data doesn't fit into memory);
- Infinite iterators/sequences;
- Produce data pipelines;

## Code example

5_generator_pipeline.py

## What's a coroutine?

coroutine (https://docs.python.org/3/glossary.html#term-coroutine)
>    Coroutines are a more generalized form of subroutines. Subroutines are
>    entered at one point and exited at another point. Coroutines can be
>    entered, exited, and resumed at many different points. They can be
>    implemented with the `async def` statement.

## Generator coroutine

In python, generators which use `yield` expressions (`value = (yield v)`), i.e.
they not only generate values, but accept them too - are called generator
coroutines.

## send(), throw(), close() methods

- `send()` - sends values to generator;
- `throw()` - sends exceptions to generator;
- `close()` - sends `GeneratorExit` to generator;

## Code example

6_send_echo.py
7_coroutine_decorator.py

## Coroutines are consumers

- Generators generate data;
- Coroutines consume data;

It's best not to mix these patterns in one function;

## Coroutine pipelines

We can create coroutine pipeline and push data through it - a reverse to
generator pipelines.

8_coroutine_pipeline.py

IMHO more readable

From: https://docs.python.org/3.10/reference/expressions.html?highlight=yield#yield-expressions
>    All of this makes generator functions quite similar to coroutines; they
>    yield multiple times, they have more than one entry point and their
>    execution can be suspended. The only difference is that a generator
>    function cannot control where the execution should continue after it
>    yields; the control is always transferred to the generator’s caller.

## Yield from

9_yield_from.py

## Event loops

All these tools allow to implement cooperative multitasking event loop

10_scheduler.py

## Resources

- https://docs.python.org/3.10/library/asyncio.html
- https://www.python.org/dev/peps/pep-3153/
- https://www.python.org/dev/peps/pep-3156/
- https://www.python.org/dev/peps/pep-0492/
- https://www.python.org/dev/peps/pep-0342/
- https://www.python.org/dev/peps/pep-0380/
- https://www.integralist.co.uk/posts/python-generators/
- https://docs.python.org/3.10/reference/datamodel.html#coroutines
- https://docs.python.org/3.10/whatsnew/3.3.html#pep-380
- https://docs.python.org/3.4/library/asyncio-task.html?highlight=coroutine#asyncio.coroutine
- https://stackoverflow.com/a/8088242
- https://dev.to/codemouse92/dead-simple-python-generators-and-coroutines-21ll
- https://python.plainenglish.io/build-your-own-event-loop-from-scratch-in-python-da77ef1e3c39
- https://iximiuz.com/en/posts/from-callback-hell-to-async-await-heaven/
- https://dgkim5360.github.io/blog/python/2017/09/basics-of-python-coroutines/
- http://www.dabeaz.com/coroutines/Coroutines.pdf
- http://www.dabeaz.com/generators/Generators.pdf
- http://www.dabeaz.com/finalgenerator/FinalGenerator.pdf
- https://www.youtube.com/watch?v=CRPnkTv1phs - Petr Viktorin: Building an async event loop
- https://www.youtube.com/watch?v=2ukHDGLr9SI - Getting started with event loops: the magic of select
- https://www.youtube.com/watch?v=LFDE-4aayuM - Event Loops without the network
- https://medium.com/fullstackai/concurrency-in-python-cooperative-vs-preemptive-scheduling-5feaed7f6e53
- https://www.youtube.com/watch?v=tSLDcRkgTsY - Python tricks: Demystifying async, await, and asyncio
